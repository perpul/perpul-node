var Perpul = require('../lib/index');

var perpul = new Perpul({
    url: '<insert server URL>',
    port: '<insert server port default 80>',
    db: '<insert database name>',
    username: '<insert username>',
    password: '<insert password>'
});

perpul.connect(function (err) {
    if (err) { return console.log(err); }
    console.log('Connected to Perpul server.');
    var inParams = [];
    inParams.push([['is_company', '=', true],['customer', '=', true]]);
    inParams.push(['name', 'country_id', 'comment']); //fields
    inParams.push(0); //offset
    inParams.push(5); //limit
    var params = [];
    params.push(inParams);
    perpul.execute_kw('res.partner', 'search_read', params, function (err, value) {
        if (err) { return console.log(err); }
        console.log('Result: ', value);
    });
});